In French, the idiom equivalent to "throw in the towel" is "jeter l'éponge." It carries the same meaning of giving up or admitting defeat.

# Sat 29 Jun 17:13:38 CEST 2024 - what's the French idiom for "throw in the towel"? what do the French say?